<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    const CREATED = "CREATED";
    const FOR_APPROVAL = "FOR APPROVAL";
    const PUBLISHED = "PUBLISHED";
    const CLOSED = "CLOSED";

    public function projectOwner() {
        return $this->belongsTo(ProjectOwner::class, 'project_owner_id');
    }

    public function proposals()
    {
        return $this->hasMany(Proposal::class);
    }

    public function images()
    {
        return $this->hasMany(ProjectImage::class);
    }

    public function serviceTags()
    {
        return $this->hasMany(ServiceTag::class);
    }

}

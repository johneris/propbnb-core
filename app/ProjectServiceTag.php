<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectServiceTag extends Model
{

    const WAITING_FOR_PROPOSALS = "WAITING_FOR_PROPOSALS";
    const PROPOSALS_IN_NEGOTIATION = "PROPOSALS_IN_NEGOTIATION";
    const PROPOSAL_CLOSED = "PROPOSAL_CLOSED";

}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{

    private $userProjectOwnerResource;
    private $projectServiceTagResourceList;
    private $imageUrls;
    private $proposalsCount;

    public function __construct($resource, $userProjectOwnerResource,
                                $projectServiceTagResourceList, $fileUrls, $proposalsCount)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->userProjectOwnerResource = $userProjectOwnerResource;
        $this->projectServiceTagResourceList = $projectServiceTagResourceList;
        $this->imageUrls = $fileUrls;
        $this->proposalsCount = $proposalsCount;
    }

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'owner' => $this->userProjectOwnerResource,
            'name' => $this->name,
            'description' => $this->description,
            'budget' => $this->budget,
            'status' => $this->status,
            'views' => $this->views,
            'proposals_count' => $this->proposalsCount,
            'target_start_at' => $this->target_start_at,
            'location' => $this->location,
            'area' => $this->area,
            'image_urls' => $this->imageUrls,
            'tags' => $this->projectServiceTagResourceList,
        ];
    }

}

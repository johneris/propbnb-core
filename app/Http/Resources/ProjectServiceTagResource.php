<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectServiceTagResource extends JsonResource
{

    private $status;

    public function __construct($resource, $status)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->status = $status;
    }

    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'image_url' => $this->image_url,
            'status' => $this->status,
        ];
    }

}

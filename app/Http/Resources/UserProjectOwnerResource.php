<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserProjectOwnerResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'user_type' => 'project_owner',
            'name' => $this->name,
            'email' => $this->email,
            'mobile_number' => $this->mobile_number,
            'avatar_url' => $this->avatar_url,
            'description' => '',
        ];
    }

}

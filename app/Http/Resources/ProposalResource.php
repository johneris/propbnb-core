<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProposalResource extends JsonResource
{

    private $userServiceProviderResource;
    private $proposalServiceTagResourceList;
    private $fileUrls;

    public function __construct($resource, $userServiceProviderResource,
                                $projectServiceTagResourceList, $fileUrls)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->userServiceProviderResource = $userServiceProviderResource;
        $this->proposalServiceTagResourceList = $projectServiceTagResourceList;
        $this->fileUrls = $fileUrls;
    }

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'service_provider' => $this->userServiceProviderResource,
            'message' => $this->message,
            'comment_from_owner' => $this->comment_from_owner,
            'price' => $this->price,
            'status' => $this->status,
            'file_urls' => $this->fileUrls,
            'tags' => $this->proposalServiceTagResourceList,
        ];
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProjectResource;
use App\Http\Resources\ProjectServiceTagResource;
use App\Http\Resources\UserProjectOwnerResource;
use App\Project;
use App\ProjectImage;
use App\ProjectOwner;
use App\ProjectServiceTag;
use App\Proposal;
use App\ServiceProvider;
use App\ServiceTag;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth:api");
    }

    public function index()
    {
        $user = Auth::user();

        $projects = Project::all();

        $serviceProvider = ServiceProvider::where('user_id', $user->id)->get()->first();

        $projectResourceList = array();
        foreach ($projects as $project) {
            if ($serviceProvider != null) {
                $proposalsCount = Proposal::where('service_provider_id', $serviceProvider->id)
                    ->where('project_id', $project->id)->get()->count();
                // don't include if service provider that is logged in has a proposal on it
                if ($proposalsCount > 0) {
                    continue;
                }
            }
            if ($serviceProvider != null) {
                if ($project->status != Project::PUBLISHED) {
                    continue;
                }
            }
            $projectResourceList[] = $this->getProjectResource($project);
        }
        return $projectResourceList;
    }

    public function show(Project $project)
    {
        return $this->getProjectResource($project);
    }

    public function me()
    {
        $user = Auth::user();

        $projectOwner = ProjectOwner::where('user_id', $user->id)->get()->first();
        if ($projectOwner != null) {
            $projects = Project::where('project_owner_id', $projectOwner->id)->get();

            $projectResourceList = array();
            foreach ($projects as $project) {
                $projectResourceList[] = $this->getProjectResource($project);
            }
            return $projectResourceList;
        }

        $serviceProvider = ServiceProvider::where('user_id', $user->id)->get()->first();
        if ($serviceProvider != null) {
            $proposals = Proposal::where('service_provider_id', $serviceProvider->id)->get();
            $projects = array();
            foreach ($proposals as $proposal) {
                $projects[] = Project::where('id', $proposal->project_id)->get()->first();
            }

            $projectResourceList = array();
            foreach ($projects as $project) {
                $projectResourceList[] = $this->getProjectResource($project);
            }
            return $projectResourceList;
        }

        return array();
    }

    private function getProjectResource($project) {
        // Owner
        $projectOwner = ProjectOwner::where('id', $project->project_owner_id)->get()->first();
        $userProjectOwnerResource = new UserProjectOwnerResource($projectOwner);

        // Images
        $imageUrls = array();
        $projectImages = ProjectImage::where('project_id', $project->id)->get();
        foreach ($projectImages as $projectImage) {
            $imageUrls[] = $projectImage->image_url;
        }

        // Tags
        $projectServiceTagResourceList = array();
        $projectServiceTags = ProjectServiceTag::where('project_id', $project->id)->get();
        foreach ($projectServiceTags as $projectServiceTag) {
            $status = $projectServiceTag->status;
            $serviceTag = ServiceTag::where('id', $projectServiceTag->service_tag_id)->get()->first();
            $projectServiceTagResourceList[] = new ProjectServiceTagResource($serviceTag, $status);
        }

        // Proposals Count
        $proposalsCount = Proposal::where('project_id', $project->id)->get()->count();

        return new ProjectResource(
            $project, $userProjectOwnerResource,
            $projectServiceTagResourceList, $imageUrls, $proposalsCount
        );
    }

}

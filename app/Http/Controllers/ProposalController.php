<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProjectResource;
use App\Http\Resources\ProjectServiceTagResource;
use App\Http\Resources\ProposalResource;
use App\Http\Resources\ServiceTagResource;
use App\Http\Resources\UserProjectOwnerResource;
use App\Http\Resources\UserServiceProviderResource;
use App\Project;
use App\ProjectOwner;
use App\ProjectServiceTag;
use App\Proposal;
use App\ProposalFile;
use App\ProposalServiceTag;
use App\ServiceProvider;
use App\ServiceTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProposalController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth:api");
    }

    public function index(Project $project)
    {
        $user = Auth::user();

        $proposalResourceList = array();

        $serviceProvider = ServiceProvider::where('user_id', $user->id)->get()->first();

        $proposals = $project->proposals;
        foreach ($proposals as $proposal) {
            if ($serviceProvider != null) {
                // don't include if service provider that is logged doesn't own the proposal
                if ($serviceProvider->id != $proposal->service_provider_id) {
                    continue;
                }
            }
            $proposalResourceList[] = $this->getProposalResource($proposal);
        }

        return $proposalResourceList;
    }

    public function show(Proposal $proposal)
    {
        return $this->getProposalResource($proposal);
    }

    public function update(Proposal $proposal, Request $request)
    {
        if ($request->status == null) {
            return response()->json([
                'message' => 'status is required.'
            ], 422);
        }

        if ($proposal->status == Proposal::PROPOSAL_SUBMITTED_BY_SERVICE_PROVIDER) {
            if ($request->status != Proposal::REVISION_REQUESTED_BY_OWNER &&
                $request->status != Proposal::PROPOSAL_ACCEPTED_BY_OWNER) {
                return response()->json([
                    'message' => 'Invalid Step: The owner can only request for a revision or accept.'
                ], 422);
            }
        }

        if ($proposal->status == Proposal::REVISION_REQUESTED_BY_OWNER) {
            if ($request->status != Proposal::PROPOSAL_SUBMITTED_BY_SERVICE_PROVIDER) {
                return response()->json([
                    'message' => 'Invalid Step: You can only go back to submitting another revision before proceeding.'
                ], 422);
            }
        }

        if ($proposal->status == Proposal::PROPOSAL_ACCEPTED_BY_OWNER) {
            if ($request->status != Proposal::SERVICE_FEE_PAID_BY_SERVICE_PROVIDER) {
                return response()->json([
                    'message' => 'Invalid Step: The service provider should pay the service fee.'
                ], 422);
            }
        }

        if ($proposal->status == Proposal::SERVICE_FEE_PAID_BY_SERVICE_PROVIDER) {
            if ($request->status != Proposal::SERVICE_FEE_CONFIRMED_BY_ADMIN) {
                return response()->json([
                    'message' => 'Invalid Step: The admin should verify the payment of the service provider.'
                ], 422);
            }
        }

        if ($proposal->status == Proposal::SERVICE_FEE_CONFIRMED_BY_ADMIN) {
            return response()->json([
                'message' => 'Invalid Step: You can\'t update the status after it has been closed.'
            ], 422);
        }

        if ($request->message != null) {
            if ($proposal->status != Proposal::REVISION_REQUESTED_BY_OWNER) {
                return response()->json([
                    'message' => 'Invalid: You can\'t edit the message unless a negotiation is requested by the owner.'
                ], 422);
            }
            $proposal->message = $request->message;
        }

        if ($request->comment_from_owner != null) {
            if ($proposal->status != Proposal::PROPOSAL_SUBMITTED_BY_SERVICE_PROVIDER &&
                ($request->status != Proposal::REVISION_REQUESTED_BY_OWNER ||
                    $request->status != Proposal::PROPOSAL_ACCEPTED_BY_OWNER)) {
                return response()->json([
                    'message' => 'Invalid: You can\'t edit the comment unless you request a negotiation.'
                ], 422);
            }
            $proposal->comment_from_owner = $request->comment_from_owner;
        }

        // Auto message
        if ($proposal->status == Proposal::PROPOSAL_SUBMITTED_BY_SERVICE_PROVIDER &&
            $request->status == Proposal::REVISION_REQUESTED_BY_OWNER) {
            $proposal->comment_from_owner = "Can we lower the price? Also, please change the color of the roof to red.";
        }

        if ($request->price != null) {
            if ($proposal->status != Proposal::REVISION_REQUESTED_BY_OWNER) {
                return response()->json([
                    'message' => 'Invalid: You can\'t edit the price unless a negotiation is requested by the owner.'
                ], 422);
            }
            $proposal->price = $request->price;
        }

        $proposal->status = $request->status;
        $proposal->save();


        if ($proposal->status == Proposal::PROPOSAL_SUBMITTED_BY_SERVICE_PROVIDER) {
            $this->updateProjectServiceTagStatus($proposal, ProjectServiceTag::PROPOSALS_IN_NEGOTIATION);
        }

        if ($proposal->status == Proposal::REVISION_REQUESTED_BY_OWNER) {
            $this->updateProjectServiceTagStatus($proposal, ProjectServiceTag::PROPOSALS_IN_NEGOTIATION);
        }

        if ($proposal->status == Proposal::PROPOSAL_ACCEPTED_BY_OWNER) {
            $this->updateProjectServiceTagStatus($proposal, ProjectServiceTag::PROPOSALS_IN_NEGOTIATION);
        }

        if ($proposal->status == Proposal::SERVICE_FEE_PAID_BY_SERVICE_PROVIDER) {
            $this->updateProjectServiceTagStatus($proposal, ProjectServiceTag::PROPOSALS_IN_NEGOTIATION);
        }

        if ($proposal->status == Proposal::SERVICE_FEE_CONFIRMED_BY_ADMIN) {
            $this->updateProjectServiceTagStatus($proposal, ProjectServiceTag::PROPOSAL_CLOSED);
        }

        return $this->getProposalResource($proposal);
    }

    private function updateProjectServiceTagStatus($proposal, $projectServiceTagStatus) {
        $projectServiceTags = ProjectServiceTag::where('project_id', $proposal->project_id)->get();
        foreach ($projectServiceTags as $projectServiceTag) {
            $proposalServiceTag = ProposalServiceTag::where('proposal_id', $proposal->id)
                ->where('service_tag_id', $projectServiceTag->service_tag_id)->get()->first();
            if ($proposalServiceTag != null) {
                $projectServiceTag->status = $projectServiceTagStatus;
                $projectServiceTag->save();
            }
        }
    }

    private function getProposalResource($proposal) {
        // Service Provider
        $serviceProvider = ServiceProvider::where('id', $proposal->service_provider_id)->get()->first();
        $userServiceProviderResource = new UserServiceProviderResource($serviceProvider);

        // Files
        $fileUrls = array();
        $proposalFiles = ProposalFile::where('proposal_id', $proposal->id)->get();
        foreach ($proposalFiles as $proposalFile) {
            $fileUrls[] = $proposalFile->file_url;
        }

        // Tags
        $proposalServiceTagResourceList = array();
        $proposalServiceTags = ProposalServiceTag::where('proposal_id', $proposal->id)->get();
        foreach ($proposalServiceTags as $proposalServiceTag) {
            $serviceTag = ServiceTag::where('id', $proposalServiceTag->service_tag_id)->get()->first();
            $proposalServiceTagResourceList[] = new ServiceTagResource($serviceTag);
        }

        return new ProposalResource(
            $proposal, $userServiceProviderResource,
            $proposalServiceTagResourceList, $fileUrls
        );
    }

}

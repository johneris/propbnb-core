<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;

class Proposal extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Proposal';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()
                ->hideFromIndex(),

            BelongsTo::make('Project', 'project', 'App\Nova\Project')
                ->searchable(),

            BelongsTo::make('Service Provider', 'serviceProvider', 'App\Nova\ServiceProvider')
                ->searchable(),

            Textarea::make('Message')
                ->hideFromIndex()
                ->alwaysShow(),

            Textarea::make('Comment From Owner')
                ->hideFromIndex()
                ->alwaysShow(),

            Text::make('Price')
                ->hideFromIndex(),

            Select::make('Status')
                ->options([
                    'PROPOSAL_SUBMITTED_BY_SERVICE_PROVIDER' => 'Proposal Submitted',
                    'REVISION_REQUESTED_BY_OWNER' => 'Revision Requested',
                    'PROPOSAL_ACCEPTED_BY_OWNER' => 'Proposal Accepted. Waiting for Service Fee Payment.',
                    'SERVICE_FEE_PAID_BY_SERVICE_PROVIDER' => 'Service Fee Pending Confirmation',
                    'SERVICE_FEE_CONFIRMED_BY_ADMIN' => 'Closed',])
                ->displayUsingLabels()
                ->hideFromIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}

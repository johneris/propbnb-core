<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{

    const PROPOSAL_SUBMITTED_BY_SERVICE_PROVIDER = "PROPOSAL_SUBMITTED_BY_SERVICE_PROVIDER";
    const REVISION_REQUESTED_BY_OWNER = "REVISION_REQUESTED_BY_OWNER";
    const PROPOSAL_ACCEPTED_BY_OWNER = "PROPOSAL_ACCEPTED_BY_OWNER";
    const SERVICE_FEE_PAID_BY_SERVICE_PROVIDER = "SERVICE_FEE_PAID_BY_SERVICE_PROVIDER";
    const SERVICE_FEE_CONFIRMED_BY_ADMIN = "SERVICE_FEE_CONFIRMED_BY_ADMIN";

    protected $fillable = [
        'message', 'comment_from_owner', 'price', 'status'
    ];

    public function project() {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function serviceProvider() {
        return $this->belongsTo(ServiceProvider::class, 'service_provider_id');
    }

}
<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
    });
});

Route::get('me', 'AuthController@me');

// List of all Projects available for Service Providers
Route::get('projects', 'ProjectController@index');

Route::get('projects/{project}', 'ProjectController@show');

// List of Projects created by the logged in Project Owner
Route::get('me/projects', 'ProjectController@me');

// Get Proposals with Project, accessible by Project Owner or Service Provider depending on status
Route::get('projects/{project}/proposals', 'ProposalController@index');

// Get Proposal with Project, accessible by Project Owner or Service Provider depending on status
Route::get('proposals/{proposal}', 'ProposalController@show');

// Update Proposal Status, accessible by Project Owner or Service Provider depending on status
Route::put('proposals/{proposal}', 'ProposalController@update');

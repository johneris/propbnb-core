<?php

use App\ProjectOwner;
use App\User;
use Illuminate\Database\Seeder;

class ProjectOwnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $avatarUrlMap = array(
            'johnerisvillanueva@gmail.com' => 'https://cdn-images-1.medium.com/fit/c/120/120/1*4j8ocpo9Yh-MUBoDniXAog.png',
            'kenneth.bolico@gmail.com' => 'https://cdn-images-1.medium.com/fit/c/120/120/1*4j8ocpo9Yh-MUBoDniXAog.png',
            'HNGadian@gmail.com' => 'https://cdn-images-1.medium.com/fit/c/120/120/1*4j8ocpo9Yh-MUBoDniXAog.png',
            'gabrielaguiao@gmail.com' => 'https://cdn-images-1.medium.com/fit/c/120/120/1*4j8ocpo9Yh-MUBoDniXAog.png'
        );

        ProjectOwner::truncate();

        $users = User::all();

        foreach ($users as $user) {
            if (!array_key_exists($user->email, $avatarUrlMap)) {
                continue;
            }
            $projectOwner = new ProjectOwner();
            $projectOwner->user_id = $user->id;
            $projectOwner->name = $user->name;
            $projectOwner->email = $user->email;
            $projectOwner->mobile_number = "09169557484";
            $projectOwner->avatar_url = $avatarUrlMap[$user->email];
            $projectOwner->save();
        }
    }

}

<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::truncate();

        // Project Owners

        $user = new User();
        $user->name = "John Eris Villanueva";
        $user->email = "johnerisvillanueva@gmail.com";
        $user->password = "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi"; // password
        $user->save();

        $user = new User();
        $user->name = "Kenneth Bolico";
        $user->email = "kenneth.bolico@gmail.com";
        $user->password = "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi"; // password
        $user->save();

        $user = new User();
        $user->name = "Harold Neil Gadian";
        $user->email = "HNGadian@gmail.com";
        $user->password = "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi"; // password
        $user->save();

        $user = new User();
        $user->name = "Maria Gabriela Guiao";
        $user->email = "gabrielaguiao@gmail.com";
        $user->password = "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi"; // password
        $user->save();

        // Service Providers

        $user = new User();
        $user->name = "Exploretale Construction Firm";
        $user->email = "contact@exploretale.com";
        $user->password = "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi"; // password
        $user->save();

        $user = new User();
        $user->name = "JELP Construction";
        $user->email = "jelp.service@jelp.com";
        $user->password = "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi"; // password
        $user->save();

        $user = new User();
        $user->name = "Belle Fonte Properties";
        $user->email = "contact@bellefonte.com";
        $user->password = "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi"; // password
        $user->save();

        $user = new User();
        $user->name = "A&A Architecture";
        $user->email = "contactus@aaa.com";
        $user->password = "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi"; // password
        $user->save();

        $user = new User();
        $user->name = "Nicole Galang";
        $user->email = "nicolegalang@gmail.com";
        $user->password = "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi"; // password
        $user->save();

        $user = new User();
        $user->name = "Engineering Corp";
        $user->email = "contact@engineeringcorp.com";
        $user->password = "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi"; // password
        $user->save();

        $user = new User();
        $user->name = "George Ramos";
        $user->email = "george.ramos@gmail.com";
        $user->password = "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi"; // password
        $user->save();

        $user = new User();
        $user->name = "Building Blocks Inc";
        $user->email = "contact@buildingblocks.com";
        $user->password = "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi"; // password
        $user->save();

        $user = new User();
        $user->name = "Admin Propbnb";
        $user->email = "admin@propbnb.com";
        $user->password = "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi"; // password
        $user->save();
    }
}

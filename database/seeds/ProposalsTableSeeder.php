<?php

use App\Proposal;
use App\ProposalFile;
use App\ProposalServiceTag;
use Illuminate\Database\Seeder;

class ProposalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Proposal::truncate();
        ProposalServiceTag::truncate();
        ProposalFile::truncate();

        // Project 1 Proposals

        $projectId = 1;

        $this->createProposal($projectId, 1, 1, "", 1);
        $this->createProposal($projectId, 2, 1, "", 2);
        $this->createProposal($projectId, 3, 1, "", 3);
        $this->createProposal($projectId, 4, 2, "", 4);
        $this->createProposal($projectId, 5, 2, "", 5);
//        $this->createProposal($projectId, 6, 3, "", 6); // will be waiting for proposals
//        $this->createProposal($projectId, 7, 3, "", 7); // will be waiting for proposals
        $this->createProposal($projectId, 8, 1, "", 8);
    }

    private function createProposal($projectId, $serviceProviderId, $serviceTagId, $price, $proposalId) {
        $proposal = new Proposal();
        $proposal->service_provider_id = $serviceProviderId;
        $proposal->project_id = $projectId;
        $proposal->message = 'We have been in the industry for years. We can ensure you that we will deliver the best work we could and execute according to your needs.';
        $proposal->comment_from_owner = 'Thank you for your proposal. I will check it and get back soon!';
        $proposal->price = "PHP 10,000,000.00";
        $proposal->status = Proposal::PROPOSAL_SUBMITTED_BY_SERVICE_PROVIDER;
        $proposal->save();

        for ($i = 0; $i < 3; $i++) {
            $proposalFile = new ProposalFile();
            $proposalFile->proposal_id = $proposalId;
            $proposalFile->file_url = "https://19of32x2yl33s8o4xza0gf14-wpengine.netdna-ssl.com/wp-content/uploads/Exhibit-A-SAMPLE-CONTRACT.pdf";
            $proposalFile->save();
        }

        $projectServiceTag = new ProposalServiceTag();
        $projectServiceTag->proposal_id = $proposalId;
        $projectServiceTag->service_tag_id = $serviceTagId;
        $projectServiceTag->save();
    }

}

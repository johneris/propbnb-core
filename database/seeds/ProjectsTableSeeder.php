<?php

use App\Project;
use App\ProjectImage;
use App\ProjectServiceTag;
use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Project::truncate();
        ProjectServiceTag::truncate();
        ProjectImage::truncate();

        // Project 1

        $images = array(
            "http://cdn.home-designing.com/wp-content/uploads/2013/06/modern-neutral-living-room-4.jpg",
            "https://freshome.com/wp-content/uploads/2016/01/minimalism-freshome-1.png",
            "https://i.pinimg.com/originals/69/e6/3a/69e63a7dff529da52ce3c1e878297c67.png",
            "https://i.pinimg.com/originals/e5/09/7c/e5097c566ecb609c1937f04ce901dc5c.png",
            "https://sttiafrica.com/i/2019/07/farmhouse-small-grey-couch-decor-trends-room-for-latest-modern-living-simple-rooms-design-designs-count-sitting-ideas-spaces-kerala-furniture.png",
            "https://i.pinimg.com/originals/69/e6/3a/69e63a7dff529da52ce3c1e878297c67.png",
            "https://i.pinimg.com/originals/e5/09/7c/e5097c566ecb609c1937f04ce901dc5c.png",
            "https://sttiafrica.com/i/2019/07/farmhouse-small-grey-couch-decor-trends-room-for-latest-modern-living-simple-rooms-design-designs-count-sitting-ideas-spaces-kerala-furniture.png"
        );

        $project = new Project();
        $project->name = 'My Dream House';
        $project->project_owner_id = 1;
        $project->description = 'I am married, a working dad and have 2 children. My wife and I want to build a new home to raise our kids. Looking at around 3 bedrooms, 2 floors, a nice kitchen, garage, and other basics. We prefer a minimalist and modern feel.';
        $project->budget = "PHP 30,000,000.00";
        $project->status = Project::PUBLISHED;
        $project->views = 1050;
        $project->target_start_at = 'January 2020';
        $project->location = 'San Juan City';
        $project->area = '500 sqm';
        $project->save();

        $this->createProjectDetails($project, $images);


        // Project 2

        $images = array(
            "https://freshome.com/wp-content/uploads/2014/07/white-washed-rustic-kitchen.jpg",
            "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/farmhouse-kitchen-ideas-1559837619.jpg?crop=0.668xw:1.00xh;0.167xw,0&resize=640:*",
            "https://i.pinimg.com/originals/7e/a6/37/7ea637066735ba86a1131473678ccd3b.jpg",
            "https://freshome.com/wp-content/uploads/2014/07/white-washed-rustic-kitchen.jpg",
            "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/farmhouse-kitchen-ideas-1559837619.jpg?crop=0.668xw:1.00xh;0.167xw,0&resize=640:*",
            "https://i.pinimg.com/originals/7e/a6/37/7ea637066735ba86a1131473678ccd3b.jpg",
            "https://i.pinimg.com/originals/7e/a6/37/7ea637066735ba86a1131473678ccd3b.jpg",
        );

        $project = new Project();
        $project->project_owner_id = 2;
        $project->name = 'Condo Revamp';
        $project->description = 'I would like a rustic themed condo. I need help with the design and renovation for the kitchen and comfort room.';
        $project->budget = "PHP 2,000,000.00";
        $project->status = Project::PUBLISHED;
        $project->views = 300;
        $project->target_start_at = 'December 2019';
        $project->location = 'Shine Residences';
        $project->area = '50 sqm';
        $project->save();

        $this->createProjectDetails($project, $images);


        // Project 3

        $images = array(
            "https://cdn.instructables.com/FXN/XBM1/F5Y3YV4Z/FXNXBM1F5Y3YV4Z.LARGE.jpg?auto=webp&&frame=1&width=1024&height=1024&fit=bounds",
            "https://i.pinimg.com/originals/0c/1d/7a/0c1d7a2e4d27c226b06f80e86a1c4b9d.jpg",
            "https://atcitalia.info/wp-content/uploads/2019/03/backyard-bbq-patio-ideas-patio-ideas-backyard-ideas-backyard-barbecue-design-ideas-patio-ideas-pictures-patio-ideas-backyard-home-decorators-collection-lighting.jpg",
            "https://cdn.instructables.com/FXN/XBM1/F5Y3YV4Z/FXNXBM1F5Y3YV4Z.LARGE.jpg?auto=webp&&frame=1&width=1024&height=1024&fit=bounds",
            "https://i.pinimg.com/originals/0c/1d/7a/0c1d7a2e4d27c226b06f80e86a1c4b9d.jpg",
            "https://atcitalia.info/wp-content/uploads/2019/03/backyard-bbq-patio-ideas-patio-ideas-backyard-ideas-backyard-barbecue-design-ideas-patio-ideas-pictures-patio-ideas-backyard-home-decorators-collection-lighting.jpg",
            "https://i.pinimg.com/originals/0c/1d/7a/0c1d7a2e4d27c226b06f80e86a1c4b9d.jpg",
        );

        $project = new Project();
        $project->project_owner_id = 3;
        $project->name = 'Backyard Construction';
        $project->description = 'My backyard is around 30 sqm and it\'s just grass. I want to make it a space where I can entertain guests and have a barbeque.';
        $project->budget = "PHP 1,000,000.00";
        $project->status = Project::PUBLISHED;
        $project->views = 570;
        $project->target_start_at = 'November 2019';
        $project->location = 'Antipolo City';
        $project->area = '30 sqm';
        $project->save();

        $this->createProjectDetails($project, $images);


        // Project 4

        $images = array(
            "https://compasspools-sydney.com.au/wp-content/uploads/2018/10/Compass-Pools-Sydney_Central-Coast_West-Coast_How-much-does-a-swimming-pool-cost-1000x500.jpg",
            "https://unilock.com/wp-content/uploads/2017/07/Swimming-pool-patio-size-banner.jpg",
            "https://stonelandscapes.ca/wp-content/uploads/2018/09/Considerations.jpg",
            "https://compasspools-sydney.com.au/wp-content/uploads/2018/10/Compass-Pools-Sydney_Central-Coast_West-Coast_How-much-does-a-swimming-pool-cost-1000x500.jpg",
            "https://unilock.com/wp-content/uploads/2017/07/Swimming-pool-patio-size-banner.jpg",
            "https://stonelandscapes.ca/wp-content/uploads/2018/09/Considerations.jpg",
            "https://unilock.com/wp-content/uploads/2017/07/Swimming-pool-patio-size-banner.jpg",
        );

        $project = new Project();
        $project->project_owner_id = 4;
        $project->name = 'Swimming Pool';
        $project->description = 'I want to have a swimming pool in the backyard for my kids and I to enjoy. I also want to entertain guests in the same area.';
        $project->budget = "PHP 1,800,000.00";
        $project->status = Project::FOR_APPROVAL;
        $project->views = 205;
        $project->target_start_at = 'February 2020';
        $project->location = 'Shine Residences';
        $project->area = '50 sqm';
        $project->save();


        $this->createProjectDetails($project, $images);
    }

    private function createProjectDetails($project, $images) {
        $status = ProjectServiceTag::WAITING_FOR_PROPOSALS;

        foreach ($images as $image) {
            $projectImage = new ProjectImage();
            $projectImage->project_id = $project->id;
            $projectImage->image_url = $image;
            $projectImage->save();
        }

        $projectServiceTag = new ProjectServiceTag();
        $projectServiceTag->project_id = $project->id;
        $projectServiceTag->service_tag_id = 1;
        if ($project->id == 1) {
            $status = ProjectServiceTag::PROPOSALS_IN_NEGOTIATION;
        }
        $projectServiceTag->status = $status;
        $projectServiceTag->save();

        $projectServiceTag = new ProjectServiceTag();
        $projectServiceTag->project_id = $project->id;
        $projectServiceTag->service_tag_id = 2;
        if ($project->id == 1) {
            $status = ProjectServiceTag::PROPOSALS_IN_NEGOTIATION;
        }
        $projectServiceTag->status = $status;
        $projectServiceTag->save();

        $projectServiceTag = new ProjectServiceTag();
        $projectServiceTag->project_id = $project->id;
        $projectServiceTag->service_tag_id = 3;
        if ($project->id == 1) {
            $status = ProjectServiceTag::WAITING_FOR_PROPOSALS;
        }
        $projectServiceTag->status = $status;
        $projectServiceTag->save();
    }

}

<?php

use App\ServiceTag;
use Illuminate\Database\Seeder;

class ServiceTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ServiceTag::truncate();

        $data = array(
            'Contractor' => 'https://image.flaticon.com/icons/svg/619/619034.svg',
            'Architect' => 'https://image.flaticon.com/icons/svg/2132/2132749.svg',
            'Engineer' => 'https://image.flaticon.com/icons/svg/2132/2132752.svg',
        );

        while($element = current($data)) {
            $serviceTag = new ServiceTag();
            $serviceTag->name = key($data);
            $serviceTag->image_url = $data[$serviceTag->name];
            $serviceTag->save();
            next($data);
        }
    }
}

<?php

use App\ServiceProvider;
use App\User;
use Illuminate\Database\Seeder;

class ServiceProvidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $avatarUrlMap = array(
            'contact@exploretale.com' => 'https://www.besthomedealsph.com/uploads/2/4/2/5/24251350/114561.png?650',
            'jelp.service@jelp.com' => 'https://images.myproperties.ph/uploads/gallery/jelp_logo.jpg',
            'contact@bellefonte.com' => 'https://siva.jsstatic.com/ph/82590/images/banner/82590_banner_0_132242.jpg',
            'contactus@aaa.com' => 'https://koobsgarage.com/wp-content/uploads/2014/04/AAA-Logo-2010.png',
            'nicolegalang@gmail.com' => 'http://static1.squarespace.com/static/541a0730e4b000f167faa35c/541a081fe4b0256ed57beeff/569448b8dc5cb4e99d1ae6fc/1452736595196/?format=1500w',
            'contact@engineeringcorp.com' => 'https://i.pinimg.com/originals/6d/55/fb/6d55fb0617cd48501f95dfc05506c578.png',
            'george.ramos@gmail.com' => 'https://cms.splendidtable.org/sites/default/files/Marvin_Gapultos.jpg',
            'contact@buildingblocks.com' => 'https://www.48hourslogo.com/48hourslogo_data/2015/12/11/2015121101213732444.jpg',
        );

        $mobileNumberMap = array(
            'contact@exploretale.com' => '09175092401',
            'jelp.service@jelp.com' => '09884340924',
            'contact@bellefonte.com' => '09168572949',
            'contactus@aaa.com' => '09853591369',
            'nicolegalang@gmail.com' => '09159340691',
            'contact@engineeringcorp.com' => '09123873596',
            'george.ramos@gmail.com' => '09568161938',
            'contact@buildingblocks.com' => '09235821698',
        );

        $descriptionMap = array(
            'contact@exploretale.com' => 'Exploretale is a real estate developer in the Philippines offering affordable condo units and houses for ordinary Filipino workers and families.',
            'jelp.service@jelp.com' => 'JELP aims to elevate the ordinary Filipino\'s quality of life by providing quality services to build their home at an afforable price.',
            'contact@bellefonte.com' => 'We are a Real Estate Development Company involved in developing residential and mixed-used subdivisions providing quality and affordable homes.',
            'contactus@aaa.com' => 'We are professionals who collaborate together to produce projects that inspire people and enhance the ubran built environment.',
            'nicolegalang@gmail.com' => 'I am a professional, skilled and creative individual who can help you build your dream home',
            'contact@engineeringcorp.com' => 'We are professionals who collaborate together to produce projects that inspire people and enhance the ubran built environment',
            'george.ramos@gmail.com' => 'I am a professional, skilled and creative individual who can help you build your dream home.',
            'contact@buildingblocks.com' => 'With each new endeavor, the company continues to fulfill Filipino dreams, making it possible for more Filipinos to acquire their first home, a lasting legacy or their most desired lifestyle.',
        );

        ServiceProvider::truncate();

        $users = User::all();

        foreach ($users as $user) {
            if (!array_key_exists($user->email, $avatarUrlMap)) {
                continue;
            }
            $serviceProvider = new ServiceProvider();
            $serviceProvider->user_id = $user->id;
            $serviceProvider->name = $user->name;
            $serviceProvider->email = $user->email;
            $serviceProvider->mobile_number = $mobileNumberMap[$user->email];
            $serviceProvider->avatar_url = $avatarUrlMap[$user->email];
            $serviceProvider->description = $descriptionMap[$user->email];
            $serviceProvider->save();
        }
    }
}

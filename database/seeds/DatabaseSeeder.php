<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ServiceTagsTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProjectOwnersTableSeeder::class);
        $this->call(ServiceProvidersTableSeeder::class);
        $this->call(ProposalsTableSeeder::class);
    }
}
